import * as fs from "mz/fs";
import { MarkdownStore } from "./MarkdownStore";
import { MarkdownOutputConfig } from "../config/MarkdownOutputConfig";
import * as logger from "winston";

export class FileStore implements MarkdownStore
{
    public writeMarkdown(config: MarkdownOutputConfig, buffer: Buffer): Promise<any>
    {
        logger.info(config.id + ": writing out tasks to file://" + config.path);

        return fs.writeFile(config.path, buffer);
    }
}
