import { TaskGroup } from "../tasks/TaskGroup";
import * as _ from "lodash";
import * as logger from "winston";
import * as moment from "moment";
import { MarkdownOutputConfig } from "../config/MarkdownOutputConfig";
import { MarkdownConfig } from "../config/MarkdownConfig";
import { MarkdownItemConfig } from "../config/MarkdownItemConfig";
import { StoreWriter } from "./StoreWriter";

export class MarkdownFormatter
{
    public static write(
        config: MarkdownOutputConfig,
        groups: TaskGroup[])
        : Promise<any>
    {
        // Check our output to make sure it is defined.
        if (!config.path)
        {
            logger.error(config.id + ": The Markdown path is not defined");
            return Promise.reject("The Markdown path is not defined");
        }

        // Normalize the options.
        config.format = config.format || <MarkdownConfig>{};
        config.format.category = config.format.category ||
            "\n# <%= group %>\n\n";
        config.format.task = config.format.task ||
            "* <%= due %> <%= labels %> <%= title %>\n";

        config.format.title = this.normalizeMarkdownItemFormat(
            config.format.title,
            "<%= title %>");
        config.format.id = this.normalizeMarkdownItemFormat(
            config.format.id,
            "#<%= id %>");
        config.format.id.supress = "id";
        config.format.labels = this.normalizeMarkdownItemFormat(
        config.format.labels,
            "[<%= labels %>]");
        config.format.labels.supress = "labels";
        config.format.due = this.normalizeMarkdownItemFormat(
            config.format.due,
            "{<%= due.format('YYYY-MM-DD') %>}");
        config.format.due.supress = "due";

        // Build up a buffer going through each item.
        var contents = "";

        for (var group of groups)
        {
            // Populate the parameter.
            var groupParams = {
                group: group.title,
            };

            // Write out the category header.
            contents += this.format(config.format.category, groupParams);

            // If we have a header, then include that.
            if (group.header)
            {
                contents += group.header;
            }

            // Loop through the items in the group.
            if (group.tasks)
            {
                for (const task of group.tasks)
                {
                    // Figure out the formatting parameters.
                    let taskParams = {
                        group: group.title,
                        id: task.id,
                        title: task.title,
                        labels: task.labels.join(", "),
                        due: <moment.Moment|string|undefined>task.due
                    };

                    taskParams.id = this.format(config.format.id, taskParams);
                    taskParams.title = this.format(config.format.title, taskParams);
                    taskParams.labels = this.format(config.format.labels, taskParams);
                    taskParams.due = this.format(config.format.due, taskParams);

                    // Format the first line.
                    contents += this.format(config.format.task, taskParams)
                        .replace(/ +/g, " ");
                }

                // If we have a footer, then include that.
                if (group.footer)
                {
                    contents += group.footer;
                }
            }
        }

        // Write out the results to the buffer based on the store.
        var buffer = new Buffer(contents.trim() + "\n", "utf-8");

        return StoreWriter.writeMarkdown(config, buffer);
    }

    private static normalizeMarkdownItemFormat(
        param: string | MarkdownItemConfig | undefined,
        template: string)
        : MarkdownItemConfig
    {
        // First normalize the variables container.
        let itemFormat: MarkdownItemConfig;

        if (typeof param === "string")
        {
            itemFormat = <MarkdownItemConfig>{ template: param };
        }
        else if (param)
        {
            itemFormat = param;
        }
        else
        {
            itemFormat = <MarkdownItemConfig>{};
        }

        // Now normalize the template fields.
        itemFormat.template = itemFormat.template || template;

        // Return the resulting format.
        return itemFormat;
    }

    private static format(
        template: string | MarkdownItemConfig,
        params: any)
        : string
    {
        // If we have a string, then use that.
        if (typeof template === "string")
        {
            return _.template(template)(params);
        }

        // If we are looking for supression, then skip that.
        if (template.supress && !params[template.supress])
        {
            return "";
        }

        // Format the results and return it.
        return _.template(template.template)(params);
    }
}
