import { MarkdownOutputConfig } from "../config/MarkdownOutputConfig";

export interface MarkdownStore
{
    writeMarkdown(config: MarkdownOutputConfig, buffer: Buffer): Promise<any>;
}
