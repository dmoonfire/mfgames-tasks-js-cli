import { FileStore } from "./FileStore";
import { MarkdownStore } from "./MarkdownStore";
import { MarkdownOutputConfig } from "../config/MarkdownOutputConfig";
import { StorageType } from "../config/StorageType";
import { DropboxStore } from "./DropboxStore";

export class StoreWriter
{
    public static writeMarkdown(
        config: MarkdownOutputConfig,
        buffer: Buffer)
        : Promise<any>
    {
        // Normalize the store to always have a valid value.
        config.store = config.store || StorageType.File;

        // Figure out how we are going to write the output.
        const configStore = config.store;
        let markdownStore: MarkdownStore;

        switch (config.store)
        {
            case StorageType.File:
                markdownStore = new FileStore();
                break;

            case StorageType.Dropbox:
                markdownStore = new DropboxStore();
                break;

            default:
                throw new Error("Unknown store: " + config.store);
        }

        // Write out the file.
        return markdownStore.writeMarkdown(config, buffer);
    }
}
