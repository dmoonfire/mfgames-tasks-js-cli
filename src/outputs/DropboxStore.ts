import "isomorphic-fetch";
import { Dropbox } from "dropbox";
import { MarkdownOutputConfig } from "../config/MarkdownOutputConfig";
import { MarkdownStore } from "./MarkdownStore";
import * as logger from "winston";
import * as path from "path";


export class DropboxStore implements MarkdownStore
{
    public async writeMarkdown(config: MarkdownOutputConfig, buffer: Buffer): Promise<any>
    {
        // If we don't have an access token, we can't do anything.
        if (!config.token)
        {
            logger.error(config.id + ": cannot write to Dropbox without `token` being set");
            return Promise.resolve(undefined);
        }

        // Report what we're doing.
        logger.info(config.id + ": Writing out tasks to dropbox://" + config.path);

        // Get a Dropbox client with the access key.
        var dropbox = new Dropbox({
            accessToken: config.token,
        });

        // Get a list of files from the path to see if it exists.
        var files = await dropbox
            .filesListFolder({ path: path.dirname(config.path) });
        var searchFile = files.entries
            .filter(f => f.name === path.basename(config.path))[0];

        if (searchFile)
        {
            // The file exists, so we need to see if we have changes.
            var oldResponse: any = await dropbox.filesDownload({
                path: config.path,
            });
            var oldBuffer: string = oldResponse.fileBinary;
            var matches = oldBuffer.toString() === buffer.toString();

            if (matches)
            {
                logger.verbose(config.id + ": File already uploaded, not changing");
                return Promise.resolve(matches);
            }

            // Overwrite the existing file. We can't figure out how to get overwrite
            // to actually work in Typescript, so we chain a delete with a create.
            logger.verbose(config.id + ": Overwriting existing file");

            return dropbox
                .filesDelete({
                    path: config.path,
                })
                .then(_a => dropbox.filesUpload({
                    contents: buffer,
                    path: config.path,
                    mute: true,
                }));
        }
        else
        {
            // The file doesn't exist, so we just upload the file.
            logger.verbose(config.id + ": Creating new file");

            return dropbox.filesUpload({
                contents: buffer,
                path: config.path,
                mute: true,
            });
        }
    }
}
