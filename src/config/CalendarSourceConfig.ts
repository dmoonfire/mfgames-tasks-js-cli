import { CommonCalendarSourceConfig } from "./CommonCalendarSourceConfig";
import { SourceType } from "./SourceType";

export interface CalendarSourceConfig extends CommonCalendarSourceConfig
{
    /** A constant key to identify GitHub sources. */
    type: SourceType.Calendar;
}
