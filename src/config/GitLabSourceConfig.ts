import { CommonSourceConfig } from "./CommonSourceConfig";
import { SourceType } from "./SourceType";

export interface GitLabSourceConfig extends CommonSourceConfig
{
    /** A constant key to identify GitLab sources. */
    type: SourceType.GitLab;

    /**
     * The URL to the Gitlab server such as "https://gitlab.com",
     * the default.
     */
    url: string | undefined;

    /** The token for the user in question. */
    token: string;
}
