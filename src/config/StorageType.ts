export enum StorageType
{
    /** Indicates the output file is a file system path. Default. */
    File = "file",

    /** Indicates the output file is a dropbox path. */
    Dropbox = "dropbox",
}
