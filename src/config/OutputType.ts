/**
 * Defines the known type of output formats.
 */
export enum OutputType
{
    Markdown = "markdown",
}
