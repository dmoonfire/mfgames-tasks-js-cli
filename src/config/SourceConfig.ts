import { GitHubSourceConfig } from "./GitHubSourceConfig";
import { GitLabSourceConfig } from "./GitLabSourceConfig";
import { CalendarSourceConfig } from "./CalendarSourceConfig";
import { CalendarBudgetSourceConfig } from "./CalendarBudgetSourceConfig";

/**
 * A tagged union of all known source configuration data.
 */

 export type SourceConfig =
    GitLabSourceConfig |
    GitHubSourceConfig |
    CalendarSourceConfig |
    CalendarBudgetSourceConfig;
