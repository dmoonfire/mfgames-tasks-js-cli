import { CommonSourceConfig } from "./CommonSourceConfig";
import { SourceType } from "./SourceType";

export interface GitHubSourceConfig extends CommonSourceConfig
{
    /** A constant key to identify GitHub sources. */
    type: SourceType.GitHub;

    /** The URL to the GitHub server such as "https://api.github.com". */
    url: string;

    /** The token for the user in question. */
    token: string;
}
