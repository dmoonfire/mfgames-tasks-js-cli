import { MarkdownOutputConfig } from "./MarkdownOutputConfig";

/** Tagged union that represents all output configurations. */
export type OutputConfig = MarkdownOutputConfig;
