import { SourceType } from "./SourceType";

/**
 * Common signature for all configuration sources.
 */
export interface CommonSourceConfig
{
    id: string;
    type: SourceType;
}
