export interface MarkdownItemConfig
{
    template: string | undefined;

    /**
     * Supress all text if the given variable is blank.
     */
    supress: string | undefined;
}
