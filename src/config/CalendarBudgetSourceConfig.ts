import { CalendarBudgetCategoryConfig } from "./CalendarBudgetCategoryConfig";
import { CommonCalendarSourceConfig } from "./CommonCalendarSourceConfig";
import { SourceType } from "./SourceType";

export interface CalendarBudgetSourceConfig extends CommonCalendarSourceConfig
{
    /** A constant key to identify GitHub sources. */
    type: SourceType.CalendarBudget;

    /** The categories to break down the budget into. */
    categories: CalendarBudgetCategoryConfig[];

    /**
     * Format the results as a table instead of a list of tasks.
     * Defaults to false.
     */
    asTable?: boolean | undefined;
}
