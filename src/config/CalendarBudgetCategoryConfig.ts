export interface CalendarBudgetCategoryConfig
{
    /**
     * How much to average the amounts over. This is used to get
     * a per month or per paycheck average. If it isn't defined or zero,
     * then total up the results.
     */
    avg: number | undefined;

    /** Until a specific day of the month. */
    dayOfMonth: number[] | undefined;

    /**
     * How far into the future to collect dates. This needs to be in the
     * format of "X days" or "Y months". Defaults to "1 months".
     */
    for: string | undefined;

    /** The name to display as the category. */
    title: string;
}
