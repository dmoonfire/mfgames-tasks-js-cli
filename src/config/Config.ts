import { OutputConfig } from "./OutputConfig";
import { SourceConfig } from "./SourceConfig";

/**
 * Interface to describe the top-level data.
 */
export interface Config
{
    sources: SourceConfig[];
    outputs: OutputConfig[];
}
