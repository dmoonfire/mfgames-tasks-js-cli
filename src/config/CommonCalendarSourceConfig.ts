import { CommonSourceConfig } from "./CommonSourceConfig";

export interface CommonCalendarSourceConfig extends CommonSourceConfig
{
    /**
     * How far into the future to collect dates. This needs to be in the
     * format of "X days" or "Y months". Defaults to "1 months".
     */
    for: string | undefined;

    /** The sort key to use. If this is undefined, then use title. */
    sort: string | undefined;

    /** The name to display as the category. */
    title: string;

    /**
     * The URL to the iCalendar file such as
     * "https://calendar.google.com/something.ics".
     */
    url: string;

    /**
     * Use the due dates for the calendar date. Defaults to true.
     */
    useDueDates?: boolean | undefined;

    /**
     * Prefix the due date at the beginning of the line.
     * Defaults to false.
     */
    prefixDate?: boolean | undefined;
}
