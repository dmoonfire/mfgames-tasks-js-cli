import { MarkdownItemConfig } from "./MarkdownItemConfig";

/**
 * Defines the options for formatting output in Markdown files. This uses
 * the lodash template for the variables.
 */
export interface MarkdownConfig
{
    /**
     * Defines the lodash format string for a group category.
     *
     * The following fields are available:
     *
     * `group`: The formatted group name.
     *
     * This defaults to `\n# <%= group %>\n\n`.
     * Whitespace is trimed before and after the line with a final newline
     * included.
     */
    category: string | undefined;

    /**
     * Defines the lodash format string for the first line.
     *
     * The following fields are available:
     *
     * `title`: The issue title after being formatted by `titleFormat`.
     * `labels`: A comma-separated list of labels.
     * `due`: The due date in `YYYY-MM-DD` format.
     *
     * Default is `* <%= title %>\n`.
     */
    task: string | undefined;

    /**
     * An optional formatting for the title for use with a lodash template
     * with the same parameters as the `first` parameter. Defaults to
     * `<%= title %>`.
     */
    title: string | MarkdownItemConfig | undefined;

    /**
     * An optional formatting for the list of labels for use with a
     * lodash template using the same parameters as the `task` parameter.
     * Defaults to `[<%= labels %>]` with a supression of `labels`.
     */
    labels: string | MarkdownItemConfig | undefined;

    /**
     * An optional formatting for due date for use with a
     * lodash template using the same parameters as the `task` parameter.
     * Defaults to `{<%= due.format('YYYY-MM-DD;) %>}` with a supression
     * of `due`. This is originally a Moment but will be a string in
     * the task formatting.
     */
    due: string | MarkdownItemConfig | undefined;

    /**
     * An optional formatting for the issue number for use with a
     * lodash template using the same parameters as the `task` parameter.
     * Defaults to `#<%= number %>` with a supression
     * of `number`.
     */
    id: string | MarkdownItemConfig | undefined;
}
