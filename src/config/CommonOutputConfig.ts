import { OutputType } from "./OutputType";

export interface CommonOutputConfig
{
    type: OutputType;
    id: string;
}
