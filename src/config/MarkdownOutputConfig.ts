import { CommonOutputConfig } from "./CommonOutputConfig";
import { OutputType } from "./OutputType";
import { StorageType } from "./StorageType";
import { MarkdownConfig } from "./MarkdownConfig";

export interface MarkdownOutputConfig extends CommonOutputConfig
{
    type: OutputType.Markdown;

    /**
     The store used to keep the files. Defaults to FileStore.File.
     */
    store: StorageType | undefined;

    /**
     * The path to the output file.
     */
    path: string;

    /**
     * The optional access token needed for some output APIs.
     */
    token: string | undefined;

    /**
     * Defines the optional formatting options for issues.
     */
    format: MarkdownConfig | undefined;
}
