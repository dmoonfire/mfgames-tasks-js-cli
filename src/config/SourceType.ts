/**
 * Defines the known type of sources of issues and tasks.
 */
export enum SourceType
{
    GitLab = "gitlab",
    GitHub = "github",
    Calendar = "icalendar",
    CalendarBudget = "icalendar-budget",
}
