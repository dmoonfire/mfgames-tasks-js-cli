import { Config } from "./config/Config";
import * as fs from "mz/fs";
import * as yaml from "js-yaml";
import * as yargs from "yargs";

export function setup(args: yargs.Arguments): yargs.Arguments
{
    return args
        .option("config", {
            type: "string",
            describe: "The configuration file",
            normalize: true
        });
}

export async function load(args: any): Promise<Config>
{
    var buffer = await fs.readFile(args.config);
    var config: Config | undefined = <Config>yaml.safeLoad(buffer.toString());

    if (!config)
    {
        throw new Error("Could not parse YAML: " + args.config);
    }

    return Promise.resolve(config);
}
