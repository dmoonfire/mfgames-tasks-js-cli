import * as moment from "moment";

export interface Task
{
    id: string | undefined;
    title: string;
    url: string | undefined;
    start: moment.Moment | undefined;
    due: moment.Moment | undefined;
    labels: string[];
}
