import { Task } from "./Task";

export interface TaskGroup
{
    /**
     * Formatted name of the group.
     */
    title: string;

    /**
     * The sort key used to order the groups in the file.
     */
    sort: string;

    /**
     * A list of tasks inside the group.
     */
    tasks: Task[];

    /**
     * The base URL for accessing the group.
     */
    url: string | undefined;

    /**
     * An optional header text for the group.
     */
    header?: string | undefined;

    /**
     * An optional footer text for the group.
     */
    footer?: string | undefined;
}
