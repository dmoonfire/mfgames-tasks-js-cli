import { GitLabProject } from "./GitLabProject";
import { GitLabProjectLookup } from "./GitLabProjectLookup";
import { GitLabSourceConfig } from "../config/GitLabSourceConfig";
import { Source } from "./Source";
import { Task } from "../tasks/Task";
import { TaskGroup } from "../tasks/TaskGroup";
import * as logger from "winston";
import * as moment from "moment";
import * as Request from "request-promise-native";

export class GitLabSource implements Source
{
    constructor(private config: GitLabSourceConfig) {}

    private get url(): string
    {
        return this.config.url || "https://gitlab.com";
    }

    public async load(): Promise<TaskGroup[]>
    {
        // Report what we're starting.
        logger.info(this.config.id + ": Loading GitLab tasks from " + this.url);

        // Get a list of all the to do items.
        let projects: GitLabProjectLookup = {};
        let groups: TaskGroup[] = [];
        const issues = await this.getList(
            "issues?state=opened&scope=assigned-to-me");

        for (var issue of issues)
        {
            // Pull out the components we need.
            var issueName = issue.title;
            var issueStartDate = undefined;
            var issueDueDate = issue.due_date;
            var issueUrl = issue.web_url;
            var issueNumber = issue.iid;

            // Get the milestone information.
            if (issue.milestone)
            {
                issueStartDate = issueStartDate || issue.milestone.start_date;
                issueDueDate = issueDueDate || issue.milestone.due_date;
            }

            // If we haven't reached the start date, then skip it because we don't
            // need to see it yet.
            if (issueStartDate)
            {
                const now = moment();
                const start = moment(issueStartDate);

                if (now < start) {
                    logger.debug("Skipping milestone that hasn't started: " + issueName);
                    continue;
                }
            }

            // Figure out what project we need.
            var projectId = issue.project_id;

            if (!(projectId in projects))
            {
                const projectJson = await this.get("projects/" + projectId);
                const projectName = projectJson.name_with_namespace;
                const projectUrl = projectJson.web_url;
                const owner = projectName.split(/ /)[0];
                const project = projectName.split(/ /)[2];
                const group: TaskGroup = {
                    title: `${owner} / ${project}`,
                    sort: `${owner} / ${project}`,
                    url: projectUrl,
                    tasks: [],
                };
                const proj: GitLabProject = {
                    group: group,
                    archived: projectJson.archived,
                };

                projects[projectId] = proj;

                if (proj.archived)
                {
                    logger.debug("Skipping archived project: " + projectName);
                }
                else
                {
                    logger.verbose("Loaded project " + projectId + ": " + projectName);
                    groups.push(group);
                }
            }

            const project = projects[projectId];

            if (project.archived)
            {
                continue;
            }

            // Create a task for this project.
            var task: Task = {
                id: issueNumber,
                title: issueName,
                url: issueUrl,
                start: issueStartDate ? moment(issueStartDate) : undefined,
                due: issueDueDate ? moment(issueDueDate) : undefined,
                labels: issue.labels.sort(),
            };

            project.group.tasks.push(task);
        }

        // Return the resulting groups.
        return Promise.resolve(groups);
    }

    /**
     * Formats a request for a GitLab server.
     */
    private req(
        endpoint: string,
        resolveWithFullResponse: boolean = false,
        body: string | undefined = undefined,
        qs: string | undefined = undefined,
        formData: string | undefined = undefined): any
    {
        let params: any = {
            url: `${this.url}/api/v4/${endpoint}`,
            headers: {
                "private-token": this.config.token,
            },
            json: true,
            resolveWithFullResponse: resolveWithFullResponse,
        };

        if (body) params.body = body;
        if (qs) params.qs = qs;
        if (formData) params.formData = formData;

        return params;
    }

    private get(
        endpoint: string,
        fullRequest: boolean = false)
    {
        return Request.get(this.req(endpoint, fullRequest));
    }

    private async getList(endpoint: string)
    {
        // Get the first response from the server.
        logger.verbose("Request first page: " + endpoint);
        var first = await Request.get(this.req(endpoint, true));
        var list: any[] = first.body;

        // See if we have any additional pages to process.
        var pageCount = first.headers["x-total-pages"];

        for (var i = 2; i <= pageCount; i++)
        {
            // Request the additional pages.
            var newEndpoint = endpoint + "&page=" + i;
            logger.verbose("Request page " + i + ": " + newEndpoint);
            var nextItems = await Request.get(this.req(newEndpoint, false));
            list = list.concat(nextItems);
        }

        // Return the resulting list.
        return Promise.resolve(list);
    }
}
