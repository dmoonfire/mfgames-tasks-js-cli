import { TaskGroup } from "../tasks/TaskGroup";

export interface Source
{
    load(): Promise<TaskGroup[]>;
}
