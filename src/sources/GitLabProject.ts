import { TaskGroup } from "../tasks/TaskGroup";

export interface GitLabProject
{
    group: TaskGroup;
    archived: boolean;
}
