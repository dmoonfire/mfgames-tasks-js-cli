import { GitHubProject } from "./GitHubProject";
import { GitHubProjectLookup } from "./GitHubProjectLookup";
import { GitHubSourceConfig } from "../config/GitHubSourceConfig";
import { Source } from "./Source";
import { Task } from "../tasks/Task";
import { TaskGroup } from "../tasks/TaskGroup";
import * as logger from "winston";
import * as moment from "moment";
import * as Request from "request-promise-native";

/**
 * Encapsulates the logic for pulling tasks from GitHub.
 */
export class GitHubSource implements Source
{
    constructor(private config: GitHubSourceConfig) { }

    private get url(): string
    {
        return this.config.url || "https://api.github.com";
    }

    public async load(): Promise<TaskGroup[]>
    {
        // Report what we're doing.
        logger.info(this.config.id + ": Loading GitHub tasks from " + this.url);

        // Get a list of all the to do items.
        let projects: GitHubProjectLookup = {};
        let groups: TaskGroup[] = [];
        const issues = await this.get("issues?state=open&filter=assigned");

        for (var issue of issues)
        {
            // Pull out the components we need.
            var issueName = issue.title;
            var issueStartDate = undefined;
            var issueDueDate = issue.due_date;
            var issueUrl = issue.url;
            var number = issue.number;

            // Get the milestone information.
            if (issue.milestone)
            {
                issueDueDate = issueDueDate || issue.milestone.due_on;
            }

            // If we haven't reached the start date, then skip it because we don't
            // need to see it yet.
            if (issueStartDate)
            {
                const now = moment();
                const start = moment(issueStartDate);

                if (now < start) {
                    logger.debug("Skipping milestone that hasn't started: " + issueName);
                    continue;
                }
            }

            // Figure out what project we need.
            var projectName = issue.repository.full_name.replace("/", " / ");

            if (!(projectName in projects))
            {
                const owner = projectName.split(/ /)[0];
                const project = projectName.split(/ /)[2];

                const group: TaskGroup = {
                    title: `${owner} / ${project}`,
                    sort: `${owner} / ${project}`,
                    url: issue.repository_url,
                    tasks: [],
                };
                const proj: GitHubProject = {
                    group: group
                };

                projects[projectName] = proj;

                logger.verbose("Loaded project: " + projectName);
                groups.push(group);
            }

            const project = projects[projectName];

            // Create a task for this project.
            var task: Task = {
                id: number,
                title: issueName,
                url: issueUrl,
                start: issueStartDate ? moment(issueStartDate) : undefined,
                due: issueDueDate ? moment(issueDueDate) : undefined,
                labels: issue.labels.map((l: any) => l.name).sort(),
            };

            project.group.tasks.push(task);
        }

        // Return the resulting groups.
        return Promise.resolve(groups);
    }

    /**
     * Formats a request for a GitHub server.
     */
    private req(
        endpoint: string,
        resolveWithFullResponse: boolean = false,
        body: string | undefined = undefined,
        qs: string | undefined = undefined,
        formData: string | undefined = undefined): any
    {
        let params: any = {
            url: `${this.url}/${endpoint}`,
            headers: {
                "Authorization": "token " + this.config.token,
                "User-Agent": "mfgames-tasks",
            },
            json: true,
            resolveWithFullResponse: resolveWithFullResponse,
        };

        if (body) params.body = body;
        if (qs) params.qs = qs;
        if (formData) params.formData = formData;

        return params;
    }

    private get(
        endpoint: string,
        fullRequest: boolean = false)
    {
        return Request.get(this.req(endpoint, fullRequest));
    }
}
