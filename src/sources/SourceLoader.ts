import { Config } from "../config/Config";
import { GitHubSource } from "./GitHubSource";
import { GitLabSource } from "./GitLabSource";
import { CalendarSource } from "./CalendarSource";
import { CalendarBudgetSource } from "./CalendarBudgetSource";
import { Source } from "./Source";
import { SourceType } from "../config/SourceType";
import { TaskGroup } from "../tasks/TaskGroup";
import * as logger from "winston";

export class SourceLoader
{
    static async load(config: Config): Promise<TaskGroup[]>
    {
        // Build up a list of tasks.
        var groups: TaskGroup[] = [];
        logger.info(`Loading ${config.sources.length} sources`);

        for (var configSource of config.sources)
        {
            // Get the data from the source.
            let source: Source;

            switch (configSource.type)
            {
                case SourceType.GitHub:
                    source = new GitHubSource(configSource);
                    break;

                case SourceType.GitLab:
                    source = new GitLabSource(configSource);
                    break;

                case SourceType.Calendar:
                    source = new CalendarSource(configSource);
                    break;

                case SourceType.CalendarBudget:
                    source = new CalendarBudgetSource(configSource);
                    break;

                default:
                    logger.error(`Unknown source type, skipping`);
                    continue;
            }

            // Load the source tasks.
            const tasks = await source.load();

            groups = groups.concat(tasks);
        }

        // Sort the resulting list for consistency.
        groups = groups.sort((a, b) => a.sort.localeCompare(b.sort));

        // Return the list of tasks.
        var taskCount = groups.reduce(
            (total, g) => (!!g.tasks ? g.tasks.length : 0) + total,
            0);

        logger.info(`Loaded ${groups.length} groups with ${taskCount} tasks`);

        return Promise.resolve(groups);
    }
}
