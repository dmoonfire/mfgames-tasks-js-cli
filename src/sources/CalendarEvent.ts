import { Moment } from "moment";

export class CalendarEvent
{
    constructor(public when: Moment, public title: string) {}
}
