import { GitHubProject } from "./GitHubProject";

export interface GitHubProjectLookup
{
    [name: string]: GitHubProject;
}
