import comma = require("comma-number");
import { CalendarBudgetCategory } from "./CalendarBudgetCategory";
import { CalendarBudgetSourceConfig } from "../config/CalendarBudgetSourceConfig";
import { CalendarEvent } from "./CalendarEvent";
import { CalendarSource } from "./CalendarSource";
import { Task } from "../tasks/Task";
import * as table from "markdown-table";
import * as _ from "lodash";
import * as logger from "winston";
import * as moment from "moment";

export class CalendarBudgetSource extends CalendarSource
{
    private categories: CalendarBudgetCategory[];
    private asTable: boolean;

    protected get then(): moment.Moment
    {
        const dates = this.categories.map((category) => category.thenString);
        const maxDate = _.max(dates);

        return moment(maxDate);
    }

    constructor(config: CalendarBudgetSourceConfig)
    {
        // Call the base implementation.
        super(config);
        this.asTable = !!config.asTable;

        // We need to parse the categories so we can override the
        // `config.for` with the longest range.
        this.categories = config.categories
            .map((category) => new CalendarBudgetCategory(category));
    }

    protected getHeader(events: CalendarEvent[]): Promise<string | undefined>
    {
        // If we aren't formatting this as a table, skip it.
        if (!this.asTable)
        {
            return Promise.resolve(undefined);
        }

        // Otherwise, format this as a table.
        return this
            .getCategories(events)
            .then((categories: CalendarBudgetCategory[]) => {
                return categories.map((cat: CalendarBudgetCategory) => {
                    // Pull out the values.
                    let income = cat.income;
                    let expenses = cat.expenses;
                    let net = cat.net;

                    // If we have an average, then we need to average these results.
                    if (cat.config.avg)
                    {
                        income /= cat.config.avg;
                        expenses /= cat.config.avg;
                        net /= cat.config.avg;
                    }

                    return [
                        cat.config.title,
                        cat.config.avg
                            ? "Average"
                            : cat.relative.replace("in ", "In "),
                        comma(net.toFixed(2)),
                        comma(income.toFixed(2)),
                        comma(expenses.toFixed(2)),
                    ];
                });
            })
            .then((lines: any[]) => {
                const headers = [[
                    "Category",
                    "Relative",
                    "Net",
                    "Income",
                    "Expenses",
                ]];

                return headers.concat(lines);
            })
            .then((lines: any[]) => {
                return table(
                    lines,
                    {
                        align: ['l', 'l', 'r', 'r', 'r'],
                    }) + "\n";
            });
    }

    protected getTasks(events: CalendarEvent[]): Promise<Task[]>
    {
        return this.asTable
            ? Promise.resolve([])
            : this.getCategories(events)
                .then((categories: CalendarBudgetCategory[]) => {
                    return categories.map((category) => <Task>{
                        id: undefined,
                        title: category.format,
                        start: undefined,
                        due: undefined,
                        labels: [],
                        url: undefined,
                    })
                });
    }

    private getCategories(events: CalendarEvent[]): Promise<CalendarBudgetCategory[]>
    {
        // Start the promise with the events we have.
        var promise: Promise<any> = Promise.resolve(events);

        // Convert the events into tasks.
        promise = promise.then((events: CalendarEvent[]) => {
            // Loop through the events since we have a lot of
            // work to do with each one.
            for (const event of events)
            {
                // Parse out the components of the string.
                var matches = event.title.match(/\$(-?[\d\.,]+) (.*)$/);

                if (!matches)
                {
                    logger.warn(this.config.id + ": cannot parse '" + event.title + "'");
                    continue;
                }

                const amount = parseFloat(matches[1].replace(/,/, ""));

                // For every event, we have to see which categories
                // it also applies to.
                for (const category of this.categories)
                {
                    // See if we apply to this one. If we don't, skip it.
                    if (!event.when.isBefore(category.then))
                    {
                        continue;
                    }

                    // Slot the values into the appropriate category.
                    category.add(amount);
                }
            }

            // Convert the categories into tasks.
            return this.categories
                .filter((category) => !!category.format);
        });

        // Return the resulting promise.
        return promise;
    }
}
