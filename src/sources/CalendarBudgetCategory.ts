import comma = require("comma-number");
import * as moment from "moment";
import { CalendarBudgetCategoryConfig } from "../config/CalendarBudgetCategoryConfig";

export class CalendarBudgetCategory
{
    public income = 0;
    public expenses = 0;
    public then: moment.Moment;

    constructor(public config: CalendarBudgetCategoryConfig) {
        this.then = this.getThen();
    }

    public get relative(): string
    {
        return this.config.dayOfMonth
            ? moment().startOf("day").to(this.then)
            : "";
    }

    public get forThen(): moment.Moment
    {
        // Just ignore it if we don't have the value, the calling
        // method will handle this.
        if (!this.config.for)
        {
            return moment().startOf("day");
        }

        // Figure out the date range.
        var args: any = {};
        const thenCount = parseFloat(this.config.for.split(/\s+/)[0]);
        const thenUnits = this.config.for.split(/\s+/)[1];
        args[thenUnits.toLowerCase()] = thenCount;

        return moment().startOf("day").add(args);
    }

    public get dayOfMonthThen(): moment.Moment
    {
        // Just ignore it if we don't have the value, the calling
        // method will handle this.
        if (!this.config.dayOfMonth)
        {
            return moment().startOf("day");
        }

        // Figure out the this and next month's day of week.
        const now = moment().startOf("day");
        let next = moment().add(1, "year");

        for (const monthOffset of [0, 1])
        {
            // Add the offset to the current month to get this and
            // next month.
            const nowMonth = moment().startOf("day").add(monthOffset, "months");

            // Go through the days of the week.
            for (const dayOfMonth of this.config.dayOfMonth)
            {
                const then = moment({
                    year: nowMonth.year(),
                    month: nowMonth.month(),
                    day: dayOfMonth,
                });

                if (then.isSameOrAfter(now) && then.isBefore(next))
                {
                    next = then;
                }
            }
        }

        // We have a valid next now. Move it to midnight of that
        // day and return the results.
        return next;
    }

    private getThen(): moment.Moment
    {
        // If we have a `for`, then the duration is easy.
        if (this.config.for)
        {
            return this.forThen;
        }

        // If we have until day of month, it is a bit more complicated.
        return this.dayOfMonthThen;
    }

    public get thenString(): string
    {
        return this.then.format("YYYY-MM-DD");
    }

    public get net(): number
    {
        return this.income - this.expenses;
    }

    public get format(): string | undefined
    {
        // Pull out the values.
        let income = this.income;
        let expenses = this.expenses;
        let net = this.net;

        // If we have an average, then we need to average these results.
        if (this.config.avg)
        {
            income /= this.config.avg;
            expenses /= this.config.avg;
            net /= this.config.avg;
        }

        // Add in the amounts, skipping any zeros.
        var parts: string[] = [income, expenses, net]
            .map((amount: number) => amount.toFixed(2))
            .map((fixed: string) => comma(fixed));

        if (parts[0] === "0.00" && parts[1] === "0.00")
        {
            return undefined;
        }

        // Figure out the title which gets the day from midnight
        // to midnight.
        let title = this.config.dayOfMonth
            ? this.config.title + " (" + this.relative + ")"
            : this.config.title;

        return title + ": $" + parts[2] + " = " + parts[0] + " - " + parts[1];
    }

    public add(amount: number): void
    {
        if (amount < 0)
        {
            this.expenses += -amount;
        }
        else
        {
            this.income += amount;
        }
    }
}
