import { GitLabProject } from "./GitLabProject";

export interface GitLabProjectLookup
{
    [id: number]: GitLabProject;
}
