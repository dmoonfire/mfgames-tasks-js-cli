const ical = require("ical");
import { CommonCalendarSourceConfig } from "../config/CommonCalendarSourceConfig";
import { CalendarEvent } from "./CalendarEvent";
import { promisify } from "util";
import { Source } from "./Source";
import { Task } from "../tasks/Task";
import { TaskGroup } from "../tasks/TaskGroup";
import * as logger from "winston";
import * as moment from "moment";

export class CalendarSource implements Source
{
    constructor(protected config: CommonCalendarSourceConfig)
    {
        this.config = config;
    }

    protected get useDueDates(): boolean
    {
        return this.config.useDueDates === undefined
            ? true
            : this.config.useDueDates;
    }

    protected get prefixDate(): boolean
    {
        return this.config.prefixDate === undefined
            ? false
            : this.config.prefixDate;
    }

    protected get for(): string
    {
        return this.config.for || "1 months";
    }

    protected get then(): moment.Moment
    {
        const thenCount = parseFloat(this.for.split(/\s+/)[0]);
        const thenType = this.for.split(/\s+/)[1];
        const thenArgs: any = {};
        thenArgs[thenType.toLowerCase()] = thenCount;

        logger.debug(this.config.id + ": looking from now until " + thenCount + " " + thenType);

        return moment().add(thenArgs).startOf("day");
    }

    public async load(): Promise<TaskGroup[]>
    {
        // Make sure we have a valid configuration.
        if (!this.config.url)
        {
            logger.error(this.config.id + ": missing `url`, cannot retrieve");
            return Promise.resolve([]);
        }

        // Report what we're starting.
        logger.info(this.config.id + ": loading iCalendar from " + this.config.url);

        // We need to know what date ranges we are working with.
        const now = moment().startOf("day");
        const then = this.then;

        logger.info(this.config.id + ": looking from "
            + now.format("YYYY-MM-DD HH:mm")
            + " to "
            + then.format("YYYY-MM-DD HH:mm"));

        // Figure out how we are going to get the iCal file.
        let promise: Promise<any>;

        if (this.config.url.startsWith("file:///"))
        {
            // Load from the disk.
            promise = Promise.resolve(ical.parseFile(this.config.url.substr(7)))
        }
        else
        {
            // Load from the network.
            const icalFromUrl = promisify(ical.fromURL);

            promise = icalFromUrl(this.config.url, {})
        }

        // We have the calendar, so loop through each one to find the
        // dates that are applicable for this range.
        promise = promise.then((cal: any) => {
            var events: any[] = [];

            for (const key in cal) {
                if (cal.hasOwnProperty(key))
                {
                    // Figure out if we care about this object type.
                    const entry = cal[key];
                    const entryType = entry.type;

                    if (entryType === "VEVENT")
                    {
                        events.push(entry);
                    }
                }
            }

            return events;
        });

        // Parse through the events and pull out the date and titles for
        // what we care about, expanding the repeating rules as needed.
        promise = promise.then((events: any[]) => {
            var results: CalendarEvent[] = [];

            for (const event of events)
            {
                // Pull out the calendar event.
                const start = moment(event.start);
                const rrule = event.rrule;
                const title = event.summary;

                // If rrule is undefined, then this is a non-repeating
                // event and we need to just see if it is within the range.
                if (rrule)
                {
                    // The `rrule` is actually a `RRule` from the `rrule`
                    // package. It has the ability to expand repeating rules
                    // into specific dates.
                    const expanded = rrule.between(now.toDate(), then.toDate());

                    for (const when of expanded)
                    {
                        results.push(new CalendarEvent(moment(when), title));
                    }
                }
                else
                {
                    // Check for the non-repeating in range.
                    if (now.isSameOrBefore(start) && then.isAfter(start))
                    {
                        results.push(new CalendarEvent(start, title));
                    }
                }
            }

            return results;
        });

        // Pass this into an extendable function to process the
        // events and turn them into tasks.
        const groupPromises: Promise<any>[] = [];

        groupPromises.push(
            promise.then((events: CalendarEvent[]) =>
                this.getTasks(events)));
        groupPromises.push(
            promise.then((events: CalendarEvent[]) =>
                this.getHeader(events)));
        groupPromises.push(
            promise.then((events: CalendarEvent[]) =>
                this.getFooter(events)));

        promise = Promise.all(groupPromises);

        // Wrap the entire thing in a task group.
        promise = promise.then((results: any[]) => {
            const tasks: Task[] = results[0];
            const header: string = results[1];
            const footer: string = results[2];

            if (tasks.length > 0 || header || footer)
            {
                return [<TaskGroup>{
                    title: this.config.title,
                    sort: this.config.sort || this.config.title,
                    tasks: tasks,
                    header: header,
                    footer: footer,
                }];
            }

            return [];
        });

        // Return the resulting groups.
        return promise;
    }

    protected getHeader(_: CalendarEvent[]): Promise<string | undefined>
    {
        return Promise.resolve(undefined);
    }

    protected getFooter(_: CalendarEvent[]): Promise<string | undefined>
    {
        return Promise.resolve(undefined);
    }

    protected getTasks(events: CalendarEvent[]): Promise<Task[]>
    {
        // Sort the tasks so they are ordered.
        var promise: Promise<any> = Promise.resolve(events);

        promise = promise.then((events: CalendarEvent[]) => {
            events.sort((a, b) => a.when.diff(b.when));
            return events;
        });

        // Convert the events into tasks.
        promise = promise.then((events: CalendarEvent[]) => {
            return events.map(
                (event) => <Task> {
                    id: undefined,
                    title: this.prefixDate
                        ? event.when.format("YYYY-MM-DD") + ": " + event.title
                        : event.title,
                    start: undefined,
                    due: this.useDueDates
                        ? event.when
                        : undefined,
                    labels: [],
                    url: undefined,
                });
        });

        // Return the resulting promise.
        return promise;
    }
}
