import { TaskGroup } from "../tasks/TaskGroup";

export interface GitHubProject
{
    group: TaskGroup;
}
