import * as logger from "winston";
import * as moment from "moment";
import * as yargs from "yargs";

export function setup(yargs: yargs.Arguments): yargs.Arguments
{
    return yargs
        .option("log-level", {
            type: "string",
            default: "info",
            describe: "The level of verbosity of the log",
            choices: ["silly", "debug", "verbose", "info", "warn", "error"],
        })
        .option("debug", {
            type: "boolean",
            default: false,
        })
        .option("verbose", {
            type: "boolean",
            default: false,
        });
}

export function init(args: any): void
{
    if (args.debug)
    {
        args["log-level"] = "debug";
    }
    else if (args.verbose)
    {
        args["log-level"] = "verbose";
    }

    logger.configure({
        level: args["log-level"],
        transports: [
            new logger.transports.Console({
                timestamp: () => moment().format("YYYY-MM-DD HH:mm:ss"),
                colorize: true
            })
        ]
    });
}
