import { MarkdownFormatter } from "../outputs/MarkdownFormatter";
import { OutputType } from "../config/OutputType";
import { SourceLoader } from "../sources/SourceLoader";
import * as configs from "../configs";
import * as log from "../log";
import * as logger from "winston";
import * as yargs from "yargs";

// The exports are all yargs-required.
export const command = "write <config>";
export const describe = "Writes out all the outputs defined in the configuration file";

export function builder(yargs: yargs.Arguments) {
    yargs = yargs
        .describe("config", "Use the given configuration")
        .string("config")
        .require("config")
        .normalize("config");

    yargs = log.setup(yargs);
    yargs = configs.setup(yargs);

    return yargs;
};

export async function handler(args: any): Promise<any> {
    // Set up logging and configuration.
    log.init(args);
    var config = await configs.load(args);

    if (!config.outputs)
    {
        logger.error("There is no 'outputs' block the configuration file");
        return;
    }

    // We need to load all the tasks from the sources.
    var groups = await SourceLoader.load(config);

    // Go through the outputs and write out each one.
    logger.info("Writing tasks to " + config.outputs.length + " outputs");

    for (const output of config.outputs)
    {
        switch (output.type)
        {
            case OutputType.Markdown:
                await MarkdownFormatter.write(output, groups);
                break;
            default:
                throw Error("Unknown output type: " + output.type);
        }
    }

    return Promise.resolve("done");
}
