## [1.2.1](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/compare/v1.2.0...v1.2.1) (2018-08-24)


### Bug Fixes

* newline after budget table ([64aad0d](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/64aad0d))

# [1.2.0](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/compare/v1.1.2...v1.2.0) (2018-08-24)


### Bug Fixes

* adding package management ([62d1bab](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/62d1bab))
* dates should always be from midnight and exclusive of ending date ([19cbafa](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/19cbafa))


### Features

* added the ability to prefix calendar entries with ISO dates ([63893dd](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/63893dd))
* optionally format the budgets as a table ([96612b1](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/96612b1))

<a name="1.1.2"></a>
## [1.1.2](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/compare/v1.1.1...v1.1.2) (2018-07-14)


### Bug Fixes

* **calendar:** date ranges should be exclusive of the end range ([2cddb23](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/2cddb23))



<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/compare/v1.1.0...v1.1.1) (2018-07-14)



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/compare/v1.0.0...v1.1.0) (2018-07-14)


### Features

* added the ability to have a specified sort on task groups ([2133b5a](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/2133b5a))
* **calendar:** added a budgeted calendar display ([0775935](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/0775935))
* **icalendar:** added iCalendar parsing ([ff61cbc](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/commit/ff61cbc)), closes [#6](https://gitlab.com/dmoonfire/mfgames-tasks-js-cli/issues/6)
