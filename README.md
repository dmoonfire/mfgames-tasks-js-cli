MfGames Tasks
=============

> A tool for aggregating tasks and assigned issues from GitLab and GitHub.

## Setup and usage

Install `mfgames-tasks-cli` using `npm`:

```sh
npm i --global mfgames-tasks-cli
```

## Configuration

Task collection and generation is based on a single YAML configuration.

```
# This is what defines the sources to scan for outstanding tasks.
sources:
    - id:       github-dmoonfire
      type:     github
      token:    TOKEN
    - id:       gitlab-dmoonfire
      type:     gitlab
      token:    TOKEN

# This defines how the files are written out.
outputs:
    - type:     markdown
      id:       md
      path:     /path/to/output
    - type:     markdown
      id:       dropbox-markdown
      store:    dropbox
      token:    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      path:     /absolute/path/relative/to/Dropbox/root
```

The tool gathers up assigned issues from GitLab or GitHub, combines them together, and then writes them out to a Markdown file.

At the moment, the output is simplified. It is intended it will be expanded by additional controls later.

## Usage

To use it, run the following:

```
mfgames-tasks write /path/to/config.yaml
```

## Sources

The following fields are used in sources.

* `type`: The API to use to get the values. Valid choices are `gitlab`, `github`, `icalendar`, or `icalendar-budget`
* `id`: Arbitrary identifier for the source, used in some messages.
* `url`: The base URL or server for the API:
    * GitLab defaults to `https://gitlab.com`
    * GitHub defaults to `https://api.github.com`
    * iCalendar: Can be `file://`, `http://`, or `https://`
    * iCalendar Budget: Can be `file://`, `http://`, or `https://`

### GitLab and GitHub Fields

* `token`: The access token from the respective site for GitLab and GitHub

### iCalendar Fields

* `title`: The category name, only used for iCalendar.
* `sort`: An override on the sort for the file, only used for iCalendar.
* `for`: The number of days ahead to use, in the format of "7 days". "months", and "weeks" (defaults to "7 days")

### iCalendar Budget Fields

* `title`: The category name, only used for iCalendar.
* `sort`: An override on the sort for the file, only used for iCalendar.
* `categories`: A list of categories to break down the items
    * `title`: The name of the category
    * `for`: As above, from now.
    * `dayOfMonth`: A list of day of months such as `[1, 16]`
    * `avg`: If provided, averages the total amounts
* `useDueDates`: If true (default), then use the calendar date as the due date.
* `prefixDate`: If true (not default), then put an ISO (YYYY-MM-DD) date in front of the title.
* `asTable`: Format the results as a Markdown table (if true).

## Output

While there can be multiple outputs, at the moment only a simplified Markdown version is provided.

* `type`: The output format to use, the only value is "markdown".
* `id`: An arbitrary ID used for messages.
* `store`: Either `file` or `dropbox`.
* `token`: The access token for Dropbox, if the store is set to `dropbox`.
* `path`: The path to write the results to.
* `format`: An additional format object.

### Formatting

Formatting is based on Lodash template properties. It is in the `format` property and contains the following keys:

* `category`: `\n# <%= owner %> / <%= project %>\n\n`
* `task`: `* <%= due %> <%= labels %> <%= title %>\n`

The following category items are formatted from the sources:

* `owner`: `<%= owner %>`
* `project`: `<%= project %>`

The following parameters are used for task and all the formats below.

* `title`: `<%= title %>`
* `id`: `#<%= id %>`
* `labels`: `[<%= labels %>]`
* `due`: `{<%= due.format('YYYY-MM-DD') %>}`

For example:

```
outputs:
  - id: test
    format:
      category: "* <%= owner %> / <%= project %>\n"
      task: "  * <%= title %> <%= due %> <%= labels %> <%= id %>\n"
      id: "#<%= id %>"
```
